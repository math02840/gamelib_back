﻿using System;
using System.Threading.Tasks;

namespace gamelib.Model
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Subtitle { get; set; }
        public int Category { get; set; }
        public double Price { get; set; }
        public string Illustration { get; set; }
        public string Platform { get; set; }
    }
}
