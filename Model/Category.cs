﻿using System;
namespace gamelib.Model
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
