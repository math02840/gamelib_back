﻿using System;
namespace gamelib.Model
{
    public class Favourite
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public int IdGame { get; set; }
    }
}
