﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gamelib.Model;
using Microsoft.EntityFrameworkCore;

namespace gamelib.Repository
{
    public class FavouriteRepository : IFavouriteRepository
    {
        private readonly AppDbContext _context;

        public FavouriteRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Favourite> Create(Favourite favourite)
        {
            _context.Favourites.Add(favourite);
            await _context.SaveChangesAsync();
            return favourite;
        }

        public async Task Delete(int id)
        {
            var favouriteToDelete = await _context.Favourites.FindAsync(id);
            _context.Favourites.Remove(favouriteToDelete);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Favourite>> Get()
        {
            return await _context.Favourites.ToListAsync();
        }

        public async Task<Favourite> Get(int id)
        {
            return await _context.Favourites.FindAsync(id);
        }

        public async Task Update(Favourite category)
        {
            _context.Entry(category).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool isFavouriteExist(Favourite favourite)
        {
            return _context.Favourites.Any(e => e.Id == favourite.Id);
        }

        public bool isIdExist(int id)
        {
            return _context.Favourites.Any(e => e.Id == id);
        }
    }
}
