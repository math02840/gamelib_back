﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using gamelib.Model;

namespace gamelib.Repository
{
    public interface IGameRepository
    {
        Task<IEnumerable<Game>> Get();

        Task<Game> Get(int id);

        Array GetWithCategory(int categoryId);

        Task<Game> Create(Game game);

        Task Update(Game game);

        Task Delete(int id);

        bool isGameExist(Game game);

        bool isIdExist(int id);
    }
}
