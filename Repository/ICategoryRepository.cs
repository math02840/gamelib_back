﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using gamelib.Model;

namespace gamelib.Repository
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Category>> Get();

        Task<Category> Get(int id);

        Task<Category> Create(Category category);

        Task Update(Category category);

        Task Delete(int id);

        bool isCategoryExist(Category category);

        bool isIdExist(int id);
    }
}
