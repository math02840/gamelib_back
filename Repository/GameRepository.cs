﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gamelib.Model;
using Microsoft.EntityFrameworkCore;

namespace gamelib.Repository
{
    public class GameRepository : IGameRepository
    {
        private readonly AppDbContext _context;

        public GameRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Game> Create(Game game)
        {
            _context.Games.Add(game);
            await _context.SaveChangesAsync();
            return game;
        }

        public async Task Delete(int id)
        {
            var gameToDelete = await _context.Games.FindAsync(id);
            _context.Games.Remove(gameToDelete);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Game>> Get()
        {
            return await _context.Games.ToListAsync();
        }

        public async Task<Game> Get(int id)
        {
            return await _context.Games.FindAsync(id);
        }

        public Array GetWithCategory(int categoryId)
        {
            return _context.Games.Where(e => e.Category == categoryId).ToArray();
        }

        public async Task Update(Game game)
        {
            _context.Entry(game).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool isGameExist(Game game)
        {
            return _context.Games.Any(e => e.Id == game.Id);
        }

        public bool isIdExist(int id)
        {
            return _context.Games.Any(e => e.Id == id);
        }
    }
}
