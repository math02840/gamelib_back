﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using gamelib.Model;

namespace gamelib.Repository
{
    public interface IFavouriteRepository
    {
        Task<IEnumerable<Favourite>> Get();

        Task<Favourite> Get(int id);

        Task<Favourite> Create(Favourite favourite);

        Task Update(Favourite favourite);

        Task Delete(int id);

        bool isFavouriteExist(Favourite favourite);

        bool isIdExist(int id);
    }
}
