﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gamelib.Model;
using Microsoft.EntityFrameworkCore;

namespace gamelib.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly AppDbContext _context;

        public CategoryRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Category> Create(Category category)
        {
            _context.Categories.Add(category);
            await _context.SaveChangesAsync();
            return category;
        }

        public async Task Delete(int id)
        {
            var categoryToDelete = await _context.Categories.FindAsync(id);
            _context.Categories.Remove(categoryToDelete);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Category>> Get()
        {
            return await _context.Categories.ToListAsync();
        }

        public async Task<Category> Get(int id)
        {
            return await _context.Categories.FindAsync(id);
        }

        public async Task Update(Category category)
        {
            _context.Entry(category).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool isCategoryExist(Category category)
        {
            return _context.Categories.Any(e => e.Id == category.Id);
        }

        public bool isIdExist(int id)
        {
            return _context.Categories.Any(e => e.Id == id);
        }
    }
}
