﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using gamelib.Model;

namespace gamelib.Repository
{
    public interface IUserRepository
    {
        Task<IEnumerable<User>> Get();

        Task<User> Get(int id);

        Task<User> Create(User user);

        Task Update(User category);

        Task Delete(int id);

        Task<User> GetByEmail(string email);
    }
}
