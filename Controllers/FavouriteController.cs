﻿using System.Collections.Generic;
using System.Threading.Tasks;
using gamelib.Model;
using gamelib.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace gamelib.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class FavouriteController : ControllerBase
    {
        private readonly IFavouriteRepository _favouriteRepository;

        public FavouriteController(IFavouriteRepository favouriteRepository)
        {
            _favouriteRepository = favouriteRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Favourite>> GetFavourites()
        {
            return await _favouriteRepository.Get();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Favourite>> GetFavouriteById(int id)
        {
            return await _favouriteRepository.Get(id);
        }

        [HttpPost]
        public async Task<ActionResult<Category>> PostFavourite([FromBody] Favourite favourite)
        {
            var newFavourite = await _favouriteRepository.Create(favourite);
            return CreatedAtAction(nameof(GetFavouriteById), new { id = newFavourite.Id }, newFavourite);
        }

        [HttpPut]
        public async Task<ActionResult> PutFavourite([FromBody] Favourite favourite)
        {
            if (!_favouriteRepository.isFavouriteExist(favourite))
            {
                return NotFound();
            }

            await _favouriteRepository.Update(favourite);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (!_favouriteRepository.isIdExist(id))
            {
                return NotFound();
            }
            await _favouriteRepository.Delete(id);
            return Ok();
        }
    }
}
