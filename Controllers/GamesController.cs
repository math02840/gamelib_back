﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gamelib.Model;
using gamelib.Repository;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace gamelib.Controllers
{
    [Route("api/[controller]")]
    public class GamesController : ControllerBase
    {
        private readonly IGameRepository _gameRepository;

        public GamesController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Game>> GetGames()
        {
            return await _gameRepository.Get();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Game>> GetGameById(int id)
        {
            return await _gameRepository.Get(id);
        }

        [HttpGet("/category/{category}")]
        public Array GetGameByCategory(int categoryId)
        {
            return _gameRepository.GetWithCategory(categoryId);
        }

        [HttpPost]
        public async Task<ActionResult<Game>> PostGame([FromBody] Game game)
        {
            var newGame = await _gameRepository.Create(game);
            return CreatedAtAction(nameof(GetGameById), new { id = newGame.Id }, newGame);
        }

        [HttpPut]
        public async Task<ActionResult> PutGame([FromBody] Game game)
        {
            if (!_gameRepository.isGameExist(game))
            {
                return NotFound();
            }

            await _gameRepository.Update(game);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (!_gameRepository.isIdExist(id))
            {
                return NotFound();
            }
            await _gameRepository.Delete(id);
            return Ok();
        }
    }
}
