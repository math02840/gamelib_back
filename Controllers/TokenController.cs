﻿using System.Threading.Tasks;
using gamelib.Interface;
using gamelib.Model;
using gamelib.Repository;
using Microsoft.AspNetCore.Mvc;

namespace gamelib.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : Controller
    {
        private readonly IJwtTokenService _tokenService;
        private readonly IUserRepository _userRepository;

        public TokenController(IJwtTokenService tokenService, IUserRepository userRepository)
        {
            _tokenService = tokenService;
            _userRepository = userRepository;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUserById(int id)
        {
            return await _userRepository.Get(id);
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Registration([FromBody] User user)
        {
            if(ModelState.IsValid == false)
            {
                return BadRequest();
            }

            var existingUser = await _userRepository.GetByEmail(user.Email);
            if (existingUser != null)
            {
                return BadRequest("L'utilisateur existe déjà");
            }

            string passwordHash = BCrypt.Net.BCrypt.HashPassword(user.Password);
            var newUser = new User(user.Name, user.Email, passwordHash);
            var createdUser = await _userRepository.Create(newUser);
            return CreatedAtAction(nameof(GetUserById), new { id = createdUser.Id }, createdUser);
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] User user)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest();
            }

            var existingUser = await _userRepository.GetByEmail(user.Email);
            if (existingUser == null)
            {
                return BadRequest("Nom d'utilisateur incorrect");
            }

            bool verified = BCrypt.Net.BCrypt.Verify(user.Password, existingUser.Password);

            if (verified == false)
            {
                return BadRequest("Nom d'utilisateur ou mot de passe incorrect");
            }

            return Ok(new { token = GenerateToken(user.Email) });
        }

        public string GenerateToken(string email)
        {
            var token = _tokenService.BuildToken(email);
            return token;
        }
    }
}
