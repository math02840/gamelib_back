﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gamelib.Model;
using gamelib.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace gamelib.Controllers
{
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoriesController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Category>> GetCatgories()
        {
            return await _categoryRepository.Get();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Category>> GetCategoryById(int id)
        {
            return await _categoryRepository.Get(id);
        }

        [HttpPost]
        public async Task<ActionResult<Category>> PostCategory([FromBody] Category category)
        {
            var newCategory = await _categoryRepository.Create(category);
            return CreatedAtAction(nameof(GetCategoryById), new { id = newCategory.Id }, newCategory);
        }

        [HttpPut]
        public async Task<ActionResult> PutCategory([FromBody] Category category)
        {
            if (!_categoryRepository.isCategoryExist(category))
            {
                return NotFound();
            }

            await _categoryRepository.Update(category);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (!_categoryRepository.isIdExist(id))
            {
                return NotFound();
            }
            await _categoryRepository.Delete(id);
            return Ok();
        }
    }
}
