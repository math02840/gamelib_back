﻿using System;
namespace gamelib.Interface
{
    public interface IJwtTokenService
    {
        string BuildToken(string email);
    }
}
